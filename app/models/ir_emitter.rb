class IrEmitter
  UnknownActionForDevice = Class.new(StandardError)

  class << self
    def exec(command)
      raise UnknownActionForDevice unless self::ACTIONS[command]
      send_signal(self::ACTIONS[command])
    end

    def send_signal(command)
      full_command = "irsend SEND_ONCE #{self.name.downcase} #{command}"
      Rails.logger.info "IR_EMITTER: Sending Command #{full_command}"
      `#{full_command}`
    end
  end
end
