class Led
  include PiPiper

  def on
    pin.on
  end

  def off
    pin.off
  end

  def pin
    @pin ||= PiPiper::Pin.new(:pin => 18, :direction => :out)
  end
end
