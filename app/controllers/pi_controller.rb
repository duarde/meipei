class PiController < ApplicationController
  # before_action :set_blink, only: [:show, :edit, :update, :destroy]

  # GET /blinks
  def index
    # @blinks = [] #Blink.all
  end

  def execute
    item.exec(command)
    render :inline => "Item #{item} runs #{command}", :type => :text
  end

  # GET /blinks/1
  def led
    led = Led.new
    if command == 'on'
      led.on
    else
      led.off
    end
    render :inline => command, :type => :text
  end

  def screen
    Screen.exec(command)
    render :inline => command, :type => :text
  end

  def nad
    Nad.exec(command)
    render :inline => command, :type => :text
  end

  private
  def command
    params[:command]
  end

  def item
    item_name = params[:item].to_s

    begin
      item_name.classify.constantize
    rescue NameError
      raise StandardError.new("Unknown Item #{item_name}")
    end
  end
end
